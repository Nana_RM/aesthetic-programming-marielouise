heres the link to my throbber!:) http://127.0.0.1:3000/Minix3/

– What do you want to explore and/or express?

With my throbber, I wanted to express the relation between watching something online and eating a snack. So, I chose popcorn because of its relation to watching entertainment at home and out in the world (cinema, theater etc.).
Note: I feel like I have seen a popcorn throbber somewhere before, but I’m not sure where or when though. So I probably (unconsciously) got a lot of inspiration from that. 

– What are the time-related syntaxes/functions that you have used in your
program, and why have you used them in this way? How is time being
constructed in computation (refer to both the reading materials and
your coding)?

Daniel Shiffman helped me a lot on this one. I have used his method of using the angle functions under function draw();, which are the syntaxes that make my image spin continuously. As far as how time is being constructed, I am currently not fully aware, but I am excited to learn more. 

– Think about a throbber that you have encounted in digital culture, e.g. for
streaming video on YouTube or loading the latest feeds on Facebook, or waiting
for a payment transaction, and consider what a throbber communicates, and/or
hides? How might we characterize this icon differently?

I see a throbber as a “You don’t need to understand what’s going on inside me (the computer), right now. I’m working on it, and you just need to give me a moment to finish.”. So to me, a throbber essentially hides everything that is going on at the moment, while it also communicates that the user has no business understanding how the computer is working, and what it is working on exactly. 
For example when a throbber is shown during a payment transaction. Is my card info being stolen right now? Can the cashier see my card info while the computer is working? Is the computer actually saving my card info? I have no idea. And the computer is not going to tell me, so I guess I don’t need to know or I shouldn’t know. Oh well  
By adding a description of what the computer is working on, it could add some clarification and peace of mind, even though it isn’t a big deal for many. A lot of them probably already do though.
