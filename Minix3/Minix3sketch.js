let popcorn;
let angle = 0;
let angleV = 0;
let angleA = 0;

function preload() {
popcorn = loadImage("popcorn.png");
}


function setup() {
  createCanvas(windowWidth, windowHeight);
  angleMode(RADIANS);

}

function draw() {

  background('#111');

  fill(128, 128, 128);
  textSize(20);
   textStyle(BOLD);
   text('POPPING POPCORN . . .', 609, 500);
   translate(width / 2, height / 2);
   rotate(angle);
   imageMode(CENTER);
   image(popcorn, 0, 0, 120, 120);
   angle += 0.01;
}
