let pretty;
let art;
let button;
let yellow;

function setup() {
createCanvas(windowWidth, windowHeight);
background(0);
frameRate(7);
}

function draw() {

button1 = createButton("Click here to create a universe (click as much as you want!)");
button1.position(70, 70);
button1.mousePressed(art);

button2 = createButton("Click here to start over");
button2.position(70, 100);
button2.mousePressed(setup);

//  the following moves my coordinate system
//  so that 0,0 is in the middle of the canvas instead of in the left upper corner.
//  This is going to make it easier for me to create my design later
translate(width/2, height/2);
function art() {

  //the following is what makes the computer choose
//any color possible for each individual line or circle.

pretty=color(random(0,255),random(0,255),random(0,255));

  fill(pretty);
  ellipse(random(-500, 500), random(-300, 300), random(1, 1));

  fill(pretty);
  ellipse(random(-500, 500), random(-300, 300), random(1, 1));

  fill(pretty);
  ellipse(random(-500, 500), random(-300, 300), random(1, 1));

  fill(pretty);
  ellipse(random(-500, 500), random(-300, 300), random(1, 1));

  fill(pretty);
  ellipse(random(-500, 500), random(-300, 300), random(1, 1));

  fill(pretty);
  ellipse(random(-500, 500), random(-300, 300), random(1, 1));

yellow=color(255, 246, 100);

   stroke(255, 248, 142);
 line(random(-300, 300), random(-300, 300), 0, 0);

   stroke(yellow);
 line(random(-300, 300), random(-300, 300), 0, 0);

   stroke(yellow);
   line(random(-300, 300), random(-300, 300), 0, 0);

}
}


// References:
// https://youtu.be/lm8Y8TD4CTM
