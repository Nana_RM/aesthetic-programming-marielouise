
[click here for colors!<3](https://marielouisehansen.gitlab.io/aesthetic-programming-marielouise/Minix5/)

for this week's minix i decided i wanted to make something visually appealing. I personally love colors, so i decided to make an gradual explotion of colors. I let the computer decide the length (within a specific area), color and placement (also within a speicific area) of all the individual lines.

I added a loop which makes the function draw() loop multiple times per frame. I set the framerate to 8 in order for the lines to not appear too quickly. 

The rules I've set in my program are 
1. for the computer to choose any random color for the lines. 
2. For the computer to choose how (within x and y -300, 300) to place the lines
3. how fast the computer should choose 
4. how many the computer should choose at a time

These rules make the outcome random and different every time, but they also ensure that the program always runs similarly and essentially ends up with a similar outcome every time. 

Programming this program has changed my view on this kind of art. I shall note that i don't view my program as an artpiece nor myself as an artist. But it has changed how i view artists working with computers and how i view their art. I now understand that the artist (human) has way more control than what i originially thought. I also have realized that, despite the computer choosing some of the visual output of the program, there can absolutely be a tremendous amount of thoughts and meanings behind what the artist has made with the computer. In class we discussed this. Then, many students, including myself, questioned the artistry behind generative art. Now, I am of the opinion that the artistry behind a generative artpiece can be just as deep and beautiful as it is behind a painting, poem or sculpture (and other forms of art, of course).
