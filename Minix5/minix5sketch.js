function setup() {
  createCanvas(windowWidth, windowHeight);
background(0);
frameRate(8);

}

function draw() {
//  the following moves my coordinate system
//  so that 0,0 is in the middle of the canvas instead of in the left upper corner.
//  This is going to make it easier for me to create my design later.
translate(width/2, height/2);

function art() {

  //the following is what makes the computer choose
//any color possible for each individual line.
pretty=color(random(0,255),random(0,255),random(0,255));

  stroke(pretty);
 line(random(-300, 300), random(-300, 300), 0, 0);
}

//the following makes multiple lines appear at once,
//which 'speeds up' the program.
var count = 0;
 while (count < 10){
art();
count++;

 }
}
