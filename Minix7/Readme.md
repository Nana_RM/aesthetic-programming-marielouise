
Title: I wanted to create something to demonstrate the fact that just one perpetrator can create hundreds of victims

[Click here to play](https://marielouisehansen.gitlab.io/aesthetic-programming-marielouise/Minix7/)



– Describe how does/do your game/game objects work?

My game works by creating various functions that detect for example whether the “player” is on top of the “smiley” or not. If the player is within a certain distance of the smiley, it counts as being on top, which in my game is called ‘impact’. The game counts how many impacts the player has. You cannot win the game, only lose.


– Describe how you program the objects and their related attributes, and the
methods in your game.

I have one object in my game, which is the smiley/target. The smiley is programmed by creating a class, in which the different attributes are put. I have used a constructor function, in which I have put different syntax such as “this.speed = floor(random(2, 5));” in order to (somewhat randomly) decide the speed of the smileys as they move across the screen, and “this.size = floor(random(25, 35));” in order to (somewhat randomly) decide the size of the smileys.

– Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?

OOP uses objects and classes to contain data and code. Data and code define the attributes and behaviors of the object in an oop code. In “Aesthetic programming – A handbook of software studies” by Winnie Soon and Geoff Cox, it is described how the programming of objects results in a severe simplification of said objects compared to their real-life counterparts. In my program for example, the smileys represent women or other people being assaulted in public. Yet, if one was to look in my code, it would become clear that these objects are incredibly simple, and don’t hold any of the same properties of their real-life counterparts. The same goes for the clown emoji, which is supposed to represent a perpetrator. The clown emoji can only collide with the smileys. So while it is supposed to represent a human, it holds almost none of the same attributes. 

Note: My program is supposed to represent that many people can fall victim to just one person. I was inspired to make this program by the recent debate regarding crime against women in public. Many has taken part in the debate by stating the obvious, which is that not every man is a perpetrator. This is a frustrating point to make, since it is obvious and therefore not necessary, and it also, in my opinion, ruins the debate because it takes away the focus from the actual issue; one perpetrator is one perpetrator too much. So even though it isn’t every man, it is still an issue that should be considered and taken care of. That was basically what I wanted to demonstrate with my program. 
I still wanted to make my program gender neutral because women aren’t the only victims of assault in public. I also didn’t want the player to be a man, because perpetrators aren’t always men, so I made the player a clown instead, since men, women and gender binary people can portray as clowns. (And obviously, assaulting people is serious clown shit). 


references:
(I made my program by changing this program by Winnie Soon): 
https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch6_ObjectAbstraction/
