/*create a class: template/blueprint of objects
 with properties and behaviors*/


class Target {
    constructor()
    { //initalize the objects
    this.speed = floor(random(2, 5));
    //check this feature: https://p5js.org/reference/#/p5/createVector
    this.pos = new createVector(width+5, random(12, height/1.7));
    this.size = floor(random(25, 35));
    //rotate in clockwise for +ve no
    this.target_rotate = random(0, PI/20);
  //  this.emoji_size = this.size/1.8;
    }
  move() {  //moving behaviors
    this.pos.x-=this.speed;  //i.e, this.pos.x = this.pos.x - this.speed;
  }

  show() {
    push()
    translate(this.pos.x, this.pos.y);
    image(targetload, 0, 0, this.size, this.size);
    pop();
 }
}
