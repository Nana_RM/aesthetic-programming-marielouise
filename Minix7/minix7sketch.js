let playerSize = {
  w:86,
  h:89
};
let player;
let playerPosY;
let mini_height;
let min_target = 5;  //min targets on the screen
let target = [];
let score =0, lose = 0;
let keyColor = 45;

function preload(){
  player = loadImage("Clown.png");
  targetload = loadImage("target1.png");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(174, 240, 255);
  playerPosY = height/2;
  mini_height = height/2;
}
function draw() {
  background(240);
  fill(keyColor, 255);
  rect(0, height/1.5, width, 1);
  displayScore();
  checkTargetNum(); //available targets
  showTarget();
  image(player, 0, playerPosY, playerSize.w, playerSize.h);
  checkEating(); //scoring
  checkResult();
}

function checkTargetNum() {
  if (target.length < min_target) {
    target.push(new Target());
  }
}

function showTarget(){
  for (let i = 0; i <target.length; i++) {
    target[i].move();
    target[i].show();
  }
}

function checkEating() {
  //calculate the distance between each target
  for (let i = 0; i < target.length; i++) {
    let d = int(
      dist(playerSize.w/2, playerPosY+playerSize.h/2,
        target[i].pos.x, target[i].pos.y)
      );
    if (d < playerSize.w/2.5) { //close enough as if eating the tofu
      score++;
      target.splice(i,1);
    }else if (target[i].pos.x < 3) { //player missed the target
      lose++;
      target.splice(i,1);
    }
  }
}

function displayScore() {
    fill(keyColor, 160);
    textSize(17);
    text('1 player has impacted '+ score + " poor innocent smiley(s)", 10, height/1.4);
    fill(keyColor,255);
    text('PRESS the ARROW UP & DOWN key to single handedly impact innocent smileys',
    10, height/1.4+40);
}

function checkResult() {
  if (lose > score && lose > 2) {
    fill(keyColor, 255);
    textSize(26);
    text("that's enough", width/3, height/1.4);
    noLoop();
  }
}

function keyPressed() {
  if (keyCode === UP_ARROW) {
    playerPosY-=50;
  } else if (keyCode === DOWN_ARROW) {
    playerPosY+=50;
  }
  //reset if the player moves out of range
  if (playerPosY > mini_height) {
    playerPosY = mini_height;
  } else if (playerPosY < 0 - playerSize.w/2) {
    playerPosY = 0;
  }
}
